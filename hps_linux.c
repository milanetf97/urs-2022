#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>

#include "alt_generalpurpose_io.h"
#include "hps_linux.h"
#include "hwlib.h"
#include "socal/alt_gpio.h"
#include "socal/socal.h"
#include "hps_soc_system.h"
#include "timer_event.h"

uint32_t applicationDone = 0;											// Varijabla koja govori da li je kraj igre

uint32_t switchArray = 0;												// Varijabla za prekidac broj 8 (za prikaz trenutnog stanja niza)
uint32_t switchTimer = 0;												// Varijabla za prekidac broj 9 (za prikaz trenutnog stanja tajmera)
uint32_t switchError = 0;												// Varijabla za prekidac broj 10 (za prikaz broja gresaka)

bool button1Pressed = false;											// Varijabla koja govori da li je pritisnut prvi taster (slijeva)
bool button2Pressed = false;											// Varijabla koja govori da li je pritisnut drugi taster (slijeva)
bool button3Pressed = false;											// Varijabla koja govori da li je pritisnut treci taster (slijeva)
bool button4Pressed = false;											// Varijabla koja govori da li je pritisnut cetvrti taster (slijeva)

uint32_t errorCounter = 0;												// Varijabla koja broji pogresne pritiske tastera
uint32_t userProgress = 0;												// Varijabla koja govori na kojem elementu niza je korisnik trenutno
uint32_t arrayInputMethodSelection = 0;									// Varijabla za selekciju nacina unosa brojeva u niz
uint32_t timerCounter = 120;											// Brojac za idle tajmer, 2 min = 120 s

uint32_t inputArray[5] = {0};											// Niz od 5 brojeva iz skupa N = {1, 2, 3, 4}
uint32_t correctButtonPressOrder[5] = {0};								// Ispravan redoslijed pritiskanja tastera

timer_event_t hTimer;													// Promjenljiva koja predstavlja vremensku kontrolu za timer 120s

/*
 *	Koristi se za definisanje file descriptora i poziva se na pocetku izvrsavanja aplikacije.
 */
void open_physical_memory_device()
{
	fd_dev_mem = open("/dev/mem", O_RDWR | O_SYNC);
	if (fd_dev_mem == -1)
	{
		printf("ERROR: could not open \"/dev/mem\"...\n");
		printf("    errno = %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
}

/*
 * 	Koristi se za mapiranje periferala koji se nalaze na HPS strani Cyclone V cipa.
 */
void mmap_hps_peripherals()
{
	hps_gpio = mmap(NULL, hps_gpio_span, PROT_READ | PROT_WRITE, MAP_SHARED, fd_dev_mem, hps_gpio_ofst);
	if (hps_gpio == MAP_FAILED)
	{
		printf("ERROR: hps_gpio mmap() failed.\n");
		printf("    errno = %s\n", strerror(errno));
		close(fd_dev_mem);
		exit(EXIT_FAILURE);
	}
}

/*
 * 	Koristi se za podesavanje pocetne vrijednosti periferija sa HPS strane Cyclone V cipa.
 */
void setup_hps_gpio()
{
	/* Initialize the HPS PIO controller
	 *     Set the direction of the HPS_LED_GPIO bit to "output"
	 *     Set the direction of the HPS_KEY_N GPIO bit to "input"
	 */
	void *hps_gpio_direction = ALT_GPIO_SWPORTA_DDR_ADDR(hps_gpio);
	alt_setbits_word(hps_gpio_direction, ALT_GPIO_PIN_OUTPUT << HPS_LED_PORT_BIT);
	alt_setbits_word(hps_gpio_direction, ALT_GPIO_PIN_INPUT << HPS_KEY_N_PORT_BIT);
}

/*
 * 	Funckija koja se poziva u petlji iz main funkcije i predstavlja algoritam koji kontrolise led diodu sa HPS strane Cyclone V cipa.
 */
void handle_hps_led()
{
	void *hps_gpio_data = ALT_GPIO_SWPORTA_DR_ADDR(hps_gpio);
	void *hps_gpio_port = ALT_GPIO_EXT_PORTA_ADDR(hps_gpio);

	uint32_t hps_gpio_input = alt_read_word(hps_gpio_port) & HPS_KEY_N_MASK;

	/* HPS_KEY_N is active-low */
	bool toggle_hps_led = (~hps_gpio_input & HPS_KEY_N_MASK);

	if (toggle_hps_led)
	{
		uint32_t hps_led_value = alt_read_word(hps_gpio_data);
		hps_led_value >>= HPS_LED_PORT_BIT;
		hps_led_value = !hps_led_value;
		hps_led_value <<= HPS_LED_PORT_BIT;
		alt_replbits_word(hps_gpio_data, HPS_LED_MASK, hps_led_value);
	}
}

/*
 * 	Koristi se za mapiranje periferala koji se nalaze na FPGA strani Cyclone V cipa.
 * 	U nasem slucaju to su dugmadi, prekidaci, led diode i 7-segmentni displeji.
 * 	Koristi bazne adrese perfijerija definisanih u hps_soc_system.h fajlu.
 */
void mmap_fpga_peripherals()
{
	h2f_lw_axi_master = mmap(NULL, h2f_lw_axi_master_span, PROT_READ | PROT_WRITE, MAP_SHARED, fd_dev_mem, h2f_lw_axi_master_ofst);

	if(h2f_lw_axi_master == MAP_FAILED)
	{
		printf("ERROR: h2f_lw_axi_master mmap() failed.\n");
		printf("    errno = %s\n", strerror(errno));
		close(fd_dev_mem);
		exit(EXIT_FAILURE);
	}

	fpga_buttons = h2f_lw_axi_master + BUTTONS_0_BASE;
	fpga_hex_displays[0] = h2f_lw_axi_master + HEX_0_BASE;
	fpga_hex_displays[1] = h2f_lw_axi_master + HEX_1_BASE;
	fpga_hex_displays[2] = h2f_lw_axi_master + HEX_2_BASE;
	fpga_hex_displays[3] = h2f_lw_axi_master + HEX_3_BASE;
	fpga_hex_displays[4] = h2f_lw_axi_master + HEX_4_BASE;
	fpga_hex_displays[5] = h2f_lw_axi_master + HEX_5_BASE;
	fpga_switches = h2f_lw_axi_master + SWITCHES_0_BASE;
	fpga_leds = h2f_lw_axi_master + LEDS_0_BASE;
}

/*
 * 	Koristi se za provjeru da li je pritisnut odredjeni taster odnosno dugme.
 *
 * 	Prima jedan argument koji predstavlja redni broj dugmeta, vrijednosti koje imaju smisla su: 0, 1, 2 i 3.
 * 		0 - najnizi bit odnosno prvi taster sa desne strane
 * 		1 - drugi najnizi bit, odnosno drugi taster sa desne strane
 * 		2 - treci najnizi bit, odnosno drugi taster sa lijeve strane
 * 		3 - cetvrti najnizi bit, odnosno prvi taster sa desne strane
 * 	Vraca boolean reprezentaciju koja je true ako je trazeno dugme pritisnuto a false ako trazeno dugme nije pritisnuto.
 *
 * 	Jedan taster odgovara jednobitnoj vrijednosti, 0 predstavja situaciju kada je taster pritisnut dok 1 predstavlja situaciju kada taster nije pritisnut.
 */
bool is_fpga_button_pressed(uint32_t button_number)
{
	/* Buttons are active-low */
	return ((~alt_read_word(fpga_buttons)) & (1 << button_number));
}

/*
 * 	Koristi se za provjeru pozicija svih 10 prekidaca.
 *
 * 	Direktno vraca reprezentaciju prekidaca kao uint32_t vrijednost. Nama su zanimljivi prvih 10 bita ondnost 10 najnizih bita.
 * 	Mogu se koristiti binarni operatori kao i u funkciji is_fpga_button_pressed za provjeru manjeg broja prekidaca.
 * 	Najnizi bit predstavlja prvi prekidac sa desne strane, najvisi od prvih 10 bita predstavlja prvi prekidac sa lijeve strane.
 * 	Reprezentacije ostalih 8 bita je lako za zakljucit.
 *
 * 	Kada je prekidac u donjem polozaju, odnosno iskljucen, odgovarajuci bit ce imati vrijednost 0. U suprotnomm ce imati vriejdnost 1.
 */
uint32_t fpga_swiches_position()
{
	return alt_read_word(fpga_switches);
}

/*
 * 	Koristi se za provjeru da li je prebacen odredjeni prekidac (oznacen brojem switch_number).
 */
bool is_fpga_switch_pressed(uint32_t switch_number)
{
	/* Switches are active-high */
	return ((alt_read_word(fpga_switches)) & (1 << switch_number));
}

/*
 * 	Koristi se za promjenu vrijednosti koje su upisane u 7-segmentne displeje.
 *
 * 	Prima decimalnu vrijednost 32-bitnog broja koju prikazuje na displejima.
 */
void set_hex_displays(uint32_t value)
{
	char current_char[2] = " \0";
	char hex_counter_hex_string[HEX_DISPLAY_COUNT + 1];

	/* Get hex string representation of input value on HEX_DISPLAY_COUNT 7-segment displays */
	snprintf(hex_counter_hex_string, HEX_DISPLAY_COUNT + 1, "%0*x", HEX_DISPLAY_COUNT, (unsigned int)value);

	uint32_t hex_display_index;
	for (hex_display_index = 0; hex_display_index < HEX_DISPLAY_COUNT; hex_display_index++)
	{
		current_char[0] = hex_counter_hex_string[HEX_DISPLAY_COUNT - hex_display_index - 1];

		/* Get decimal representation for this 7-segment display */
		uint32_t number = (uint32_t)strtol(current_char, NULL, 16);

		/* Use lookup table to find active-low value representing number on the 7-segment display */
		uint32_t hex_value_to_write = hex_display_table[number];

		alt_write_word(fpga_hex_displays[hex_display_index], hex_value_to_write);
	}
}

/*
 * 	Koristi se za postavljanje pocetnih vrijednosti 7-segmentnih displeja. Pocetno stanje je predstavljeno kada su svi segmenti na displeju ugaseni.
 */
void setup_hex_displays()
{
	int i;
	/* Turn all hex displays off */
	for(i=0; i < HEX_DISPLAY_COUNT; i++)
	{
		alt_write_word(fpga_hex_displays[i], HEX_DISPLAY_CLEAR);
	}
}

/*
 * 	Funckija koja se poziva u petlji iz main funkcije i predstavlja algoritam koji kontrolise 7-segmentne displeje.
 *
 * 	Prima pokazivac na brojac. Vrijednost brojaca se salje na displeje.
 *
 * 	Ukoliko se pritisne prvo dugme (prvo desno) mjenja se izmedju inkrement i dekrement rezima rada brojaca.
 * 	Ukoliko se pritisne drugo dugme (drugo desno) restartuje se vrijednost brojaca na 0.
 */
void handle_hex_displays(uint32_t *hex_counter)
{
	static bool button_0_pressed = false;
	static bool button_1_pressed = false;
	static bool hex_up_direction = true;

	if(hex_up_direction)
	{
		(*hex_counter)++;
	}
	else
	{
		(*hex_counter)--;
	}

	set_hex_displays(*hex_counter);

	if(is_fpga_button_pressed(0))
	{
		if(!button_0_pressed)
		{
			hex_up_direction = !hex_up_direction;
			button_0_pressed = true;
		}
	}
	else
	{
		button_0_pressed = false;
	}

	if(is_fpga_button_pressed(1))
	{
		if(!button_1_pressed)
		{
			*hex_counter = 0;
			button_1_pressed = true;
		}
	}
	else
	{
		button_1_pressed = false;
	}
}

/*
 * 	Koristi se za postavljanje pocetnih vrijednosti led dioda. Pocetno stanje je predstavljeno kada su sve diode ugasene.
 */
void setup_leds()
{
	/* Turn all leds off */
	alt_write_word(fpga_leds, LEDS_CLEAR);
}

/*
 * 	Koristi se za promjenu vrijednosti koje su upisane u diode.
 *
 * 	Prima decimalnu vrijednost 32-bitnog broja koju saljemo na diode
 *
 * 	Ovom funkcijom mjenjamo vrijednosti svih 10 dioda. Kada je potrebno promjeniti vrijednosti samo malom broju dioda
 * 	bolje je koristiti binarne operatore kao u slucaju citanja vrijednosti tastera.
 */
void set_leds(uint32_t value)
{
	leds_value = value;
	alt_write_word(fpga_leds, value);
}

/*
 * 	Funckija koja se poziva u petlji iz main funkcije i predstavlja algoritam koji kontrolise led diode.
 *
 * 	Cita vrijednost prekidaca i ukoliko je doslo do promjene stanja bar jednog prekidaca nova vriejdnost prekidaca se salje na diode,
 * 	tako da diode odgovaraju prekidacima.
 */
void handle_leds()
{
	switch_value_new = fpga_swiches_position();
	if(switch_value_old != switch_value_new){
		switch_value_old = switch_value_new;
		set_leds(switch_value_new);
	}
}


/*
 * 	Koristi se za oslobadjanje memorije koju zauzimaju periferije sa FPGA strane Cyclone V cipa.
 */
void munmap_fpga_peripherals()
{
	int i;

	if(munmap(h2f_lw_axi_master, h2f_lw_axi_master_span) != 0)
	{
		printf("ERROR: h2f_lw_axi_master munmap() failed.\n");
		printf("    errno = %s\n", strerror(errno));
		close(fd_dev_mem);
		exit(EXIT_FAILURE);
	}

	h2f_lw_axi_master = NULL;
	fpga_buttons = NULL;
	fpga_switches = NULL;
	fpga_leds = NULL;

	for (i=0; i < HEX_DISPLAY_COUNT; i++)
	{
		fpga_hex_displays[i] = NULL;
	}

}

/*
 * 	Koristi se za oslobadjanje memorije koju zauzimaju periferije sa FPGA strane Cyclone V cipa.
 */
void munmap_hps_peripherals()
{
	if(munmap(hps_gpio, hps_gpio_span) != 0)
	{
		printf("ERROR: hps_gpio munmap() failed.\n");
		printf("    errno = %s\n", strerror(errno));
		close(fd_dev_mem);
		exit(EXIT_FAILURE);
	}

	hps_gpio = NULL;
}

/*
 * 	Koristi se za objedinjavanje funkcija za mapiranje periferija na Cyclone V cipu. Poziva se na pocetku izvrsavanja aplikacije nakon
 * 	open_physical_memory_device funkcije.
 */
void mmap_peripherals() {
    mmap_hps_peripherals();
    mmap_fpga_peripherals();
}

/*
 * 	Koristi se za objedinjavanje funkcija za oslobadjavanje zauzete memorije od strane periferija na Cyclone V cipu.
 * 	Poziva se na kraju izvrsavanja aplikacije ali prije close_physical_memory_device funkcije
 */
void munmap_peripherals() {
    munmap_hps_peripherals();
    munmap_fpga_peripherals();
}

/*
 *	Koristi se za oslobadjanje file descriptora i poziva se na kraju izvrsavanja aplikacije.
 */
void close_physical_memory_device()
{
	close(fd_dev_mem);
}


int main(int argc, char** argv)
{
	printf("\n\n\nUGRADJENI RACUNARSKI SISTEMI - PROJEKTNI ZADATAK\n\n");

	printf("====================POCETAK!====================\n");
	printf("================================================\n\n");

	/* Flag koji govori da li je array uspjesno popunjen */
	uint32_t arrayFilledsuccessfully = 0;

	/* Provjerava se da li je korisnik pri pokretanju unio 5 argumenata komandne linije za popunjavanje niza */
	if(argc != 6) {
		printf("Niste definisali elemente niza kroz argumente komandne linije.\n");
		do {
			printf("Na koji nacin zelite definisati elemente niza?\n");
			printf("1 - Rucno\n");
			printf("2 - Nasumicno\n");
			printf("3 - Iz fajla\n");

			printf("Unesite zeljenu opciju: ");
			scanf("%d", &arrayInputMethodSelection);
			getchar();
			printf("\n");
			switch(arrayInputMethodSelection) {
				case 1:
					arrayFilledsuccessfully = readArrayUserInput(inputArray);
					break;
				case 2:
					arrayFilledsuccessfully = readArrayRandom(inputArray);
					break;
				case 3:
					arrayFilledsuccessfully = readArrayFile(inputArray);
					break;
				default:
					printf("Pogresan unos! Dozvoljene opcije su 1, 2 i 3.\n");
					arrayFilledsuccessfully = 0;
					break;
				}
		} while(!arrayFilledsuccessfully);
	} else {
		printf("Citanje argumenata komandne linije\n");
		if(SUCCESS == readArrayCommandLineArguments(inputArray, argv)) {
			printf("Argumenti komandne linije su uspjesno procitani!\n");
		} else {
			printf("Argumenti nisu odgovarajuci!\n");
			printf("Terminacija aplikacije!\n");
			return -1;
		}
	}

	determineCorrectOrder(inputArray, correctButtonPressOrder);

	open_physical_memory_device();
	mmap_peripherals();

	setup();

	printf("Pocetak igre!\n");

    /* Formiranje vremenske kontrole za funkciju print_state_timer. */
    timer_event_set(&hTimer, 1000, timerDecrement, 0, TE_KIND_REPETITIVE);

	while(!applicationDone) {
		writeLeds(userProgress);
		writeDisplays(userProgress);
		readSwitches();
		readButtons();
		if(timerCounter == 0) {
			timerTimeout();
		}
	}

	setup();

    timer_event_kill(hTimer);

	munmap_peripherals();
	close_physical_memory_device();

	printf("=====================KRAJ!======================\n");
	printf("================================================\n\n\n");

	return 0;
}

/* Funkcija popunjava niz na osnovu argumenata komandne linije.
 * Funkcija vraca SUCCESS ako je uspjesno izvrsena ili FAILED ako je neuspjesno izvrsena */
int readArrayCommandLineArguments(uint32_t array[], char** argv) {
	int retVal = SUCCESS;
	uint32_t convertedValue;
	for (int i = 0; i < 5; i++) {
		convertedValue = atoi(argv[1 + i]);
		if (convertedValue >= 1 && convertedValue <= 4) {
			array[i] = convertedValue;
		} else {
			retVal = FAILED;
			break;
		}
	}
	return retVal;
}

/* Funkcija popunjava niz na osnovu korisnickog unosa */
int readArrayUserInput(uint32_t array[]) {
	int index = 0;
	int temp = 0;
	while(index < 5) {
		printf("Unesite array[%d] = ", index);
		scanf("%d", &temp);
		getchar();
		if(temp >= 1 && temp <= 4) {
			array[index] = temp;
			index++;
		}
	}
	return SUCCESS;
}

/* Funkcija koja popunjava niz iz fajla,
 * Funkcija vraca SUCCESS u slucaju uspjesnog izvrsavanja ili FAILED u slucaju neuspjesnog izvrsavanja */
int readArrayFile(uint32_t array[]) {
	char path[100] = {0};
	printf("Unesite apsolutnu putanju do fajla\n");
	printf("Putanja: ");
	if (fgets(path, 100, stdin) != NULL) {
		path[strcspn(path, "\n")] = 0;
		printf("\nUnijeli ste: %s\n", path);
		FILE* fp = fopen(path, "r");
		if(fp == NULL) {
			printf("Greska pri otvaranju fajla!\n\n");
			return FAILED;
		}
		uint32_t temp1, temp2, temp3, temp4, temp5;
		if(fscanf(fp, "%d %d %d %d %d", &temp1, &temp2, &temp3, &temp4, &temp5) == 5) {
			if(temp1 >= 1 && temp2 >= 1 && temp3 >= 1 && temp4 >= 1 && temp5 >= 1 && temp1 <= 4 && temp2 <= 4 && temp3 <= 4 && temp4 <= 4 && temp5 <= 4) {
				array[0] = temp1;
				array[1] = temp2;
				array[2] = temp3;
				array[3] = temp4;
				array[4] = temp5;
				fclose(fp);
			} else {
				printf("Brojevi van opsega!\n\n");
				fclose(fp);
				return FAILED;
			}
		} else {
			printf("Pogresan format fajla!\n\n");
			fclose(fp);
			return FAILED;
		}

	} else {
		printf("Greska!\n\n");
		return FAILED;
	}
	return SUCCESS;
}

/* Funkcija koja popunjava niz nasumicno */
int readArrayRandom(uint32_t array[]) {
	srand(time(NULL));
	array[0] = 1 + rand() % 4;
	array[1] = 1 + rand() % 4;
	array[2] = 1 + rand() % 4;
	array[3] = 1 + rand() % 4;
	array[4] = 1 + rand() % 4;
	return SUCCESS;
}

/* Funkcija koja na osnovu ulaznog niza, odredjuje ispravan redoslijed tastera koje korisnik treba pritisnuti */
void determineCorrectOrder(uint32_t input[], uint32_t result[]) {
	switch (input[0]) {
		case 1:
			result[0] = 2;
			break;
		case 2:
			result[0] = 2;
			break;
		case 3:
			result[0] = 3;
			break;
		case 4:
			result[0] = 4;
			break;
	}

	switch (input[1]) {
		case 1:
			result[1] = 4;
			break;
		case 2:
			result[1] = result[0];
			break;
		case 3:
			result[1] = 1;
			break;
		case 4:
			result[1] = result[0];
			break;
	}

	switch (input[2]) {
		case 1:
			result[2] = input[1];
			break;
		case 2:
			result[2] = input[0];
			break;
		case 3:
			result[2] = 3;
			break;
		case 4:
			result[2] = 4;
			break;
	}

	switch (input[3]) {
		case 1:
			result[3] = result[0];
			break;
		case 2:
			result[3] = 1;
			break;
		case 3:
			result[3] = result[1];
			break;
		case 4:
			result[3] = result[1];
			break;
	}

	switch (input[4]) {
		case 1:
			result[4] = input[0];
			break;
		case 2:
			result[4] = input[1];
			break;
		case 3:
			result[4] = input[3];
			break;
		case 4:
			result[4] = input[2];
			break;
	}
}

/* Funkcija koja gasi sve HEX displeje i LE diode */
void setup() {
	setup_hps_gpio();
	setup_hex_displays();
	setup_leds();
}

/* Funkcija na HEX displejima ispisuje FAILED */
void showFailed() {
	alt_write_word(fpga_hex_displays[0], HEX_DISPLAY_D);
	alt_write_word(fpga_hex_displays[1], HEX_DISPLAY_E);
	alt_write_word(fpga_hex_displays[2], HEX_DISPLAY_L);
	alt_write_word(fpga_hex_displays[3], HEX_DISPLAY_I);
	alt_write_word(fpga_hex_displays[4], HEX_DISPLAY_A);
	alt_write_word(fpga_hex_displays[5], HEX_DISPLAY_F);
}

/* Funkcija na HEX displejima ispisuje SUCSES */
void showSuccess() {
	alt_write_word(fpga_hex_displays[0], HEX_DISPLAY_S);
	alt_write_word(fpga_hex_displays[1], HEX_DISPLAY_E);
	alt_write_word(fpga_hex_displays[2], HEX_DISPLAY_S);
	alt_write_word(fpga_hex_displays[3], HEX_DISPLAY_C);
	alt_write_word(fpga_hex_displays[4], HEX_DISPLAY_U);
	alt_write_word(fpga_hex_displays[5], HEX_DISPLAY_S);
}

/* Funkcija na HEX displejima prikazuje 2 slova H */
void show2H() {
	alt_write_word(fpga_hex_displays[0], HEX_DISPLAY_CLEAR);
	alt_write_word(fpga_hex_displays[1], HEX_DISPLAY_CLEAR);
	alt_write_word(fpga_hex_displays[2], HEX_DISPLAY_CLEAR);
	alt_write_word(fpga_hex_displays[3], HEX_DISPLAY_CLEAR);
	alt_write_word(fpga_hex_displays[4], HEX_DISPLAY_H);
	alt_write_word(fpga_hex_displays[5], HEX_DISPLAY_H);
}

/* Funkcija na HEX displejima prikazuje 4 slova H */
void show4H() {
	alt_write_word(fpga_hex_displays[0], HEX_DISPLAY_CLEAR);
	alt_write_word(fpga_hex_displays[1], HEX_DISPLAY_CLEAR);
	alt_write_word(fpga_hex_displays[2], HEX_DISPLAY_H);
	alt_write_word(fpga_hex_displays[3], HEX_DISPLAY_H);
	alt_write_word(fpga_hex_displays[4], HEX_DISPLAY_H);
	alt_write_word(fpga_hex_displays[5], HEX_DISPLAY_H);
}

/* Funkcija na HEX displejima prikazuje 6 slova H */
void show6H() {
	alt_write_word(fpga_hex_displays[0], HEX_DISPLAY_H);
	alt_write_word(fpga_hex_displays[1], HEX_DISPLAY_H);
	alt_write_word(fpga_hex_displays[2], HEX_DISPLAY_H);
	alt_write_word(fpga_hex_displays[3], HEX_DISPLAY_H);
	alt_write_word(fpga_hex_displays[4], HEX_DISPLAY_H);
	alt_write_word(fpga_hex_displays[5], HEX_DISPLAY_H);
}

/* Funkcija koja ispisuje broj decimalno na displeje */
void write_number_to_display(uint32_t value)
{
	char current_char[2] = " \0";
	char my_string[HEX_DISPLAY_COUNT + 1] = {0};

	/* Get string representation of input value */
	snprintf(my_string, HEX_DISPLAY_COUNT + 1, "%0*d", HEX_DISPLAY_COUNT, (unsigned int)value);
	uint32_t hex_display_index;
	for (hex_display_index = 0; hex_display_index < HEX_DISPLAY_COUNT; hex_display_index++)
	{
		current_char[0] = my_string[HEX_DISPLAY_COUNT - hex_display_index - 1];

		/* Get decimal representation for this 7-segment display */
		uint32_t number = (uint32_t)strtol(current_char, NULL, 10);

		/* Use lookup table to find active-low value representing number on the 7-segment display */
		uint32_t hex_value_to_write = hex_display_table[number];

		alt_write_word(fpga_hex_displays[hex_display_index], hex_value_to_write);
	}
}

/* Funkcija koja ispisuje vrijednost tajmera na displeje */
void showTimer() {
	write_number_to_display(timerCounter);
}

/* Funkcija koja na HEX displejima prikazuje stanje niza u zavisnosti od toga do kojeg elementa niza je korisnik uspjesno dosao */
void showArray(uint32_t userProgress) {
	clearDisplays();
	switch(userProgress) {
		case 0:
			alt_write_word(fpga_hex_displays[5], hex_display_table[inputArray[0]]);
			break;
		case 1:
			alt_write_word(fpga_hex_displays[5], hex_display_table[inputArray[0]]);
			alt_write_word(fpga_hex_displays[4], hex_display_table[inputArray[1]]);
			break;
		case 2:
			alt_write_word(fpga_hex_displays[5], hex_display_table[inputArray[0]]);
			alt_write_word(fpga_hex_displays[4], hex_display_table[inputArray[1]]);
			alt_write_word(fpga_hex_displays[3], hex_display_table[inputArray[2]]);
			break;
		case 3:
			alt_write_word(fpga_hex_displays[5], hex_display_table[inputArray[0]]);
			alt_write_word(fpga_hex_displays[4], hex_display_table[inputArray[1]]);
			alt_write_word(fpga_hex_displays[3], hex_display_table[inputArray[2]]);
			alt_write_word(fpga_hex_displays[2], hex_display_table[inputArray[3]]);
			break;
		case 4:
			alt_write_word(fpga_hex_displays[5], hex_display_table[inputArray[0]]);
			alt_write_word(fpga_hex_displays[4], hex_display_table[inputArray[1]]);
			alt_write_word(fpga_hex_displays[3], hex_display_table[inputArray[2]]);
			alt_write_word(fpga_hex_displays[2], hex_display_table[inputArray[3]]);
			alt_write_word(fpga_hex_displays[1], hex_display_table[inputArray[4]]);
			break;
		default:
			break;
	}
}

/* Funkcija koja gasi sve HEX displeje */
void clearDisplays()
{
	int i;
	for(i=0; i < HEX_DISPLAY_COUNT; i++)
	{
		alt_write_word(fpga_hex_displays[i], HEX_DISPLAY_CLEAR);
	}
}


/* Ova funkcija se poziva kada korisnik napravi 3 pogresna izbora, te ce se tada oznaciti da treba zavrsiti aplikaciju */
void missionFailed() {
	timer_event_kill(hTimer);
	printf("Napravili ste 3 pogresna izbora!\n");
	writeLeds(0);
	show6H();
	sleep(5);
	showFailed();
	printf("Terminacija aplikacije!\n\n");
	sleep(5);
	applicationDone = 1;
}

/* Ova funkcija se poziva kada korisnik uspjesno dodje do kraja niza, te ce se tada oznaciti da treba zavrsiti aplikaciju */
void missionPassed() {
	timer_event_kill(hTimer);
	printf("Cestitamo!\n\n");
	showSuccess();
	writeLeds(userProgress);
	sleep(3);
	applicationDone = 1;
}

/* Funkcija koja se poziva kada istekne tajmer 120s */
void timerTimeout() {
	timer_event_kill(hTimer);
	printf("Tajmer je istekao!\n\n");
	showFailed();
	sleep(3);
	applicationDone = 1;
}

/* Funkcija vremenske kontrole koje se poziva na svake sekunde. */
void* timerDecrement (void *param)
{
	timerCounter--;
    return 0;
}

/* Funkcija koja cita stanja prekidaca */
void readSwitches() {
	switchError = is_fpga_switch_pressed(0);
	switchTimer = is_fpga_switch_pressed(1);
	switchArray = is_fpga_switch_pressed(2);
}

/* Funkcija koja u zavisnosti od korisnickog uspjeha pali odredjen broj LE dioda */
void writeLeds(uint32_t userProgress) {
	switch (userProgress) {
		case 0:
			alt_write_word(fpga_leds, LEDS_CLEAR);
			break;
		case 1:
			alt_write_word(fpga_leds, LEDS_2);
			break;
		case 2:
			alt_write_word(fpga_leds, LEDS_4);
			break;
		case 3:
			alt_write_word(fpga_leds, LEDS_6);
			break;
		case 4:
			alt_write_word(fpga_leds, LEDS_8);
			break;
		case 5:
			alt_write_word(fpga_leds, LEDS_10);
			break;
		default:
			alt_write_word(fpga_leds, LEDS_CLEAR);
			break;
	}
}

/* Funkcija koja ispisuje sadrzaj na HEX displejima u zavisnosti od polozaja prekidaca */
void writeDisplays() {
	if (1 == switchError) {
		if(0 == errorCounter) {
			clearDisplays();
		} else if (1 == errorCounter) {
			show2H();
		} else if (2 == errorCounter) {
			show4H();
		} else if (3 == errorCounter) {
			show6H();
		}
	} else if (1 == switchTimer) {
		showTimer();
	} else if (1 == switchArray) {
		showArray(userProgress);
	} else {
		showArray(userProgress);
	}
}

/* Funkcija koja reaguje na pritisak nekog tastera, te poziva funkciju koja provjerava da li je pritisnut ispravan taster */
void readButtons() {
	if(is_fpga_button_pressed(0))
	{
		if(!button4Pressed)
		{
			button4Pressed = true;
			check(correctButtonPressOrder, 4);
		}
	}
	else
	{
		button4Pressed = false;
	}

	if(is_fpga_button_pressed(1))
	{
		if(!button3Pressed)
		{
			button3Pressed = true;
			check(correctButtonPressOrder, 3);
		}
	}
	else
	{
		button3Pressed = false;
	}

	if(is_fpga_button_pressed(2))
	{
		if(!button2Pressed)
		{
			button2Pressed = true;
			check(correctButtonPressOrder, 2);
		}
	}
	else
	{
		button2Pressed = false;
	}

	if(is_fpga_button_pressed(3))
		{
			if(!button1Pressed)
			{
				button1Pressed = true;
				check(correctButtonPressOrder, 1);
			}
		}
		else
		{
			button1Pressed = false;
		}
}

/* Funkcija koja provjerava da li je pritisnut ispravan taster */
void check(uint32_t correctButtonPressOrder[], uint32_t button) {
	if(button == correctButtonPressOrder[userProgress]) {
		userProgress++;
		if(userProgress == 5) {
			missionPassed();
		}
	} else {
		errorCounter++;
		userProgress = 0;
		if(errorCounter == 3) {
			missionFailed();
		}

	}
}
